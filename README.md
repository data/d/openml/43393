# OpenML dataset: USA-Housing-Listings

https://www.openml.org/d/43393

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Craigslist is the world's largest collection of privately sold housing options, yet it's very difficult to collect all of them in the same place. I built this dataset as a means in by which to perform experimental analysis on the United States states as a whole instead of isolated urban housing markets.
Content
This data is scraped every few months, it contains most all relevant information that Craigslist provides on retail sales

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43393) of an [OpenML dataset](https://www.openml.org/d/43393). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43393/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43393/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43393/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

